'use strict';


customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">qpm-api documentation</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Type to search"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
                    <ul class="links">
                        <li class="link">
                            <a href="overview.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Overview
                            </a>
                        </li>
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>README
                            </a>
                        </li>
                                <li class="link">
                                    <a href="dependencies.html" data-type="chapter-link">
                                        <span class="icon ion-ios-list"></span>Dependencies
                                    </a>
                                </li>
                    </ul>
                </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-toggle="collapse" ${ isNormalMode ?
                                'data-target="#modules-links"' : 'data-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse " ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/AppModule.html" data-type="entity-link">AppModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-AppModule-ed81dbded231b9c80d00f9a07f0ca86a"' : 'data-target="#xs-controllers-links-module-AppModule-ed81dbded231b9c80d00f9a07f0ca86a"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-AppModule-ed81dbded231b9c80d00f9a07f0ca86a"' :
                                            'id="xs-controllers-links-module-AppModule-ed81dbded231b9c80d00f9a07f0ca86a"' }>
                                            <li class="link">
                                                <a href="controllers/AppController.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AppController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-AppModule-ed81dbded231b9c80d00f9a07f0ca86a"' : 'data-target="#xs-injectables-links-module-AppModule-ed81dbded231b9c80d00f9a07f0ca86a"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-AppModule-ed81dbded231b9c80d00f9a07f0ca86a"' :
                                        'id="xs-injectables-links-module-AppModule-ed81dbded231b9c80d00f9a07f0ca86a"' }>
                                        <li class="link">
                                            <a href="injectables/AppService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>AppService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/AuthModule.html" data-type="entity-link">AuthModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-AuthModule-f538a27f2bef5bd74d68d0d9e8cdc448"' : 'data-target="#xs-controllers-links-module-AuthModule-f538a27f2bef5bd74d68d0d9e8cdc448"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-AuthModule-f538a27f2bef5bd74d68d0d9e8cdc448"' :
                                            'id="xs-controllers-links-module-AuthModule-f538a27f2bef5bd74d68d0d9e8cdc448"' }>
                                            <li class="link">
                                                <a href="controllers/AuthController.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AuthController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-AuthModule-f538a27f2bef5bd74d68d0d9e8cdc448"' : 'data-target="#xs-injectables-links-module-AuthModule-f538a27f2bef5bd74d68d0d9e8cdc448"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-AuthModule-f538a27f2bef5bd74d68d0d9e8cdc448"' :
                                        'id="xs-injectables-links-module-AuthModule-f538a27f2bef5bd74d68d0d9e8cdc448"' }>
                                        <li class="link">
                                            <a href="injectables/AuthService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>AuthService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/UtilsService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>UtilsService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/CommonModule.html" data-type="entity-link">CommonModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/InvitesModule.html" data-type="entity-link">InvitesModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-InvitesModule-9b7455fb1e6dd305a3e6b4d2c70dd4b7"' : 'data-target="#xs-controllers-links-module-InvitesModule-9b7455fb1e6dd305a3e6b4d2c70dd4b7"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-InvitesModule-9b7455fb1e6dd305a3e6b4d2c70dd4b7"' :
                                            'id="xs-controllers-links-module-InvitesModule-9b7455fb1e6dd305a3e6b4d2c70dd4b7"' }>
                                            <li class="link">
                                                <a href="controllers/InvitesController.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">InvitesController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-InvitesModule-9b7455fb1e6dd305a3e6b4d2c70dd4b7"' : 'data-target="#xs-injectables-links-module-InvitesModule-9b7455fb1e6dd305a3e6b4d2c70dd4b7"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-InvitesModule-9b7455fb1e6dd305a3e6b4d2c70dd4b7"' :
                                        'id="xs-injectables-links-module-InvitesModule-9b7455fb1e6dd305a3e6b4d2c70dd4b7"' }>
                                        <li class="link">
                                            <a href="injectables/InviteRepository.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>InviteRepository</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/InvitesService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>InvitesService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/UtilsService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>UtilsService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/MembersModule.html" data-type="entity-link">MembersModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-MembersModule-1ccba4b40fcc4b98b8d1840d982cd70e"' : 'data-target="#xs-controllers-links-module-MembersModule-1ccba4b40fcc4b98b8d1840d982cd70e"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-MembersModule-1ccba4b40fcc4b98b8d1840d982cd70e"' :
                                            'id="xs-controllers-links-module-MembersModule-1ccba4b40fcc4b98b8d1840d982cd70e"' }>
                                            <li class="link">
                                                <a href="controllers/MembersController.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">MembersController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-MembersModule-1ccba4b40fcc4b98b8d1840d982cd70e"' : 'data-target="#xs-injectables-links-module-MembersModule-1ccba4b40fcc4b98b8d1840d982cd70e"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-MembersModule-1ccba4b40fcc4b98b8d1840d982cd70e"' :
                                        'id="xs-injectables-links-module-MembersModule-1ccba4b40fcc4b98b8d1840d982cd70e"' }>
                                        <li class="link">
                                            <a href="injectables/MemberRepository.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>MemberRepository</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/MembersService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>MembersService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/UtilsService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>UtilsService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/OrganizationsModule.html" data-type="entity-link">OrganizationsModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-OrganizationsModule-dd811b3bd1fd1b0e5a8ab807e1cba9ad"' : 'data-target="#xs-controllers-links-module-OrganizationsModule-dd811b3bd1fd1b0e5a8ab807e1cba9ad"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-OrganizationsModule-dd811b3bd1fd1b0e5a8ab807e1cba9ad"' :
                                            'id="xs-controllers-links-module-OrganizationsModule-dd811b3bd1fd1b0e5a8ab807e1cba9ad"' }>
                                            <li class="link">
                                                <a href="controllers/OrganizationsController.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">OrganizationsController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-OrganizationsModule-dd811b3bd1fd1b0e5a8ab807e1cba9ad"' : 'data-target="#xs-injectables-links-module-OrganizationsModule-dd811b3bd1fd1b0e5a8ab807e1cba9ad"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-OrganizationsModule-dd811b3bd1fd1b0e5a8ab807e1cba9ad"' :
                                        'id="xs-injectables-links-module-OrganizationsModule-dd811b3bd1fd1b0e5a8ab807e1cba9ad"' }>
                                        <li class="link">
                                            <a href="injectables/OrganizationRepository.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>OrganizationRepository</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/OrganizationsService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>OrganizationsService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/UtilsService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>UtilsService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/ProjectsModule.html" data-type="entity-link">ProjectsModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-ProjectsModule-7af52422cbae8b8466710163942a9611"' : 'data-target="#xs-controllers-links-module-ProjectsModule-7af52422cbae8b8466710163942a9611"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-ProjectsModule-7af52422cbae8b8466710163942a9611"' :
                                            'id="xs-controllers-links-module-ProjectsModule-7af52422cbae8b8466710163942a9611"' }>
                                            <li class="link">
                                                <a href="controllers/ProjectsController.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ProjectsController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-ProjectsModule-7af52422cbae8b8466710163942a9611"' : 'data-target="#xs-injectables-links-module-ProjectsModule-7af52422cbae8b8466710163942a9611"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-ProjectsModule-7af52422cbae8b8466710163942a9611"' :
                                        'id="xs-injectables-links-module-ProjectsModule-7af52422cbae8b8466710163942a9611"' }>
                                        <li class="link">
                                            <a href="injectables/ProjectRepository.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>ProjectRepository</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/ProjectsService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>ProjectsService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/UtilsService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>UtilsService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                </ul>
                </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#controllers-links"' :
                                'data-target="#xs-controllers-links"' }>
                                <span class="icon ion-md-swap"></span>
                                <span>Controllers</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="controllers-links"' : 'id="xs-controllers-links"' }>
                                <li class="link">
                                    <a href="controllers/AppController.html" data-type="entity-link">AppController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/AuthController.html" data-type="entity-link">AuthController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/InvitesController.html" data-type="entity-link">InvitesController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/MembersController.html" data-type="entity-link">MembersController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/OrganizationsController.html" data-type="entity-link">OrganizationsController</a>
                                </li>
                                <li class="link">
                                    <a href="controllers/ProjectsController.html" data-type="entity-link">ProjectsController</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#classes-links"' :
                            'data-target="#xs-classes-links"' }>
                            <span class="icon ion-ios-paper"></span>
                            <span>Classes</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="classes-links"' : 'id="xs-classes-links"' }>
                            <li class="link">
                                <a href="classes/AuthCredentialsDto.html" data-type="entity-link">AuthCredentialsDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/Company.html" data-type="entity-link">Company</a>
                            </li>
                            <li class="link">
                                <a href="classes/CreateMemberDto.html" data-type="entity-link">CreateMemberDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/CreateMemberInviteDto.html" data-type="entity-link">CreateMemberInviteDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/CreateOrganizationDto.html" data-type="entity-link">CreateOrganizationDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/CreateOrganizationInviteDto.html" data-type="entity-link">CreateOrganizationInviteDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/CreateOrganizationPreferencesDTO.html" data-type="entity-link">CreateOrganizationPreferencesDTO</a>
                            </li>
                            <li class="link">
                                <a href="classes/CreateProjectBudgetDTO.html" data-type="entity-link">CreateProjectBudgetDTO</a>
                            </li>
                            <li class="link">
                                <a href="classes/CreateProjectCompaniesDTO.html" data-type="entity-link">CreateProjectCompaniesDTO</a>
                            </li>
                            <li class="link">
                                <a href="classes/CreateProjectDto.html" data-type="entity-link">CreateProjectDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/CreateProjectLocationDTO.html" data-type="entity-link">CreateProjectLocationDTO</a>
                            </li>
                            <li class="link">
                                <a href="classes/CreateProjectMapOverlaysDTO.html" data-type="entity-link">CreateProjectMapOverlaysDTO</a>
                            </li>
                            <li class="link">
                                <a href="classes/CreateProjectPerimeterDTO.html" data-type="entity-link">CreateProjectPerimeterDTO</a>
                            </li>
                            <li class="link">
                                <a href="classes/CreateProjectScheduleDTO.html" data-type="entity-link">CreateProjectScheduleDTO</a>
                            </li>
                            <li class="link">
                                <a href="classes/CreateUserDto.html" data-type="entity-link">CreateUserDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/CreateUserOrganizationDTO.html" data-type="entity-link">CreateUserOrganizationDTO</a>
                            </li>
                            <li class="link">
                                <a href="classes/HttpExceptionFilter.html" data-type="entity-link">HttpExceptionFilter</a>
                            </li>
                            <li class="link">
                                <a href="classes/Invite.html" data-type="entity-link">Invite</a>
                            </li>
                            <li class="link">
                                <a href="classes/LatLng.html" data-type="entity-link">LatLng</a>
                            </li>
                            <li class="link">
                                <a href="classes/MapOverlay.html" data-type="entity-link">MapOverlay</a>
                            </li>
                            <li class="link">
                                <a href="classes/MatchConstraint.html" data-type="entity-link">MatchConstraint</a>
                            </li>
                            <li class="link">
                                <a href="classes/Member.html" data-type="entity-link">Member</a>
                            </li>
                            <li class="link">
                                <a href="classes/NonExistentCollectionException.html" data-type="entity-link">NonExistentCollectionException</a>
                            </li>
                            <li class="link">
                                <a href="classes/Organization.html" data-type="entity-link">Organization</a>
                            </li>
                            <li class="link">
                                <a href="classes/Perimeter.html" data-type="entity-link">Perimeter</a>
                            </li>
                            <li class="link">
                                <a href="classes/Project.html" data-type="entity-link">Project</a>
                            </li>
                            <li class="link">
                                <a href="classes/ProjectBudget.html" data-type="entity-link">ProjectBudget</a>
                            </li>
                            <li class="link">
                                <a href="classes/ProjectLocation.html" data-type="entity-link">ProjectLocation</a>
                            </li>
                            <li class="link">
                                <a href="classes/ProjectMember.html" data-type="entity-link">ProjectMember</a>
                            </li>
                            <li class="link">
                                <a href="classes/ProjectSchedule.html" data-type="entity-link">ProjectSchedule</a>
                            </li>
                            <li class="link">
                                <a href="classes/TaskStatusValidationPipe.html" data-type="entity-link">TaskStatusValidationPipe</a>
                            </li>
                            <li class="link">
                                <a href="classes/UpdateMemberDto.html" data-type="entity-link">UpdateMemberDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/UpdateMemberInviteDto.html" data-type="entity-link">UpdateMemberInviteDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/UpdateOrganizationDto.html" data-type="entity-link">UpdateOrganizationDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/UpdateOrganizationInviteDto.html" data-type="entity-link">UpdateOrganizationInviteDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/UpdateProjectDto.html" data-type="entity-link">UpdateProjectDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/UpdateUserDto.html" data-type="entity-link">UpdateUserDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/User.html" data-type="entity-link">User</a>
                            </li>
                            <li class="link">
                                <a href="classes/UserRepository.html" data-type="entity-link">UserRepository</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#injectables-links"' :
                                'data-target="#xs-injectables-links"' }>
                                <span class="icon ion-md-arrow-round-down"></span>
                                <span>Injectables</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="injectables-links"' : 'id="xs-injectables-links"' }>
                                <li class="link">
                                    <a href="injectables/AppService.html" data-type="entity-link">AppService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/AttachCurrentUserMiddleware.html" data-type="entity-link">AttachCurrentUserMiddleware</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/AuthService.html" data-type="entity-link">AuthService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/InviteRepository.html" data-type="entity-link">InviteRepository</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/InvitesService.html" data-type="entity-link">InvitesService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/MemberRepository.html" data-type="entity-link">MemberRepository</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/MembersService.html" data-type="entity-link">MembersService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/OrganizationRepository.html" data-type="entity-link">OrganizationRepository</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/OrganizationsService.html" data-type="entity-link">OrganizationsService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ProjectRepository.html" data-type="entity-link">ProjectRepository</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ProjectsService.html" data-type="entity-link">ProjectsService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/TimeoutInterceptor.html" data-type="entity-link">TimeoutInterceptor</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/UtilsService.html" data-type="entity-link">UtilsService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/WrapResponseInterceptor.html" data-type="entity-link">WrapResponseInterceptor</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#guards-links"' :
                            'data-target="#xs-guards-links"' }>
                            <span class="icon ion-ios-lock"></span>
                            <span>Guards</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="guards-links"' : 'id="xs-guards-links"' }>
                            <li class="link">
                                <a href="guards/InviteApiKeyGuard.html" data-type="entity-link">InviteApiKeyGuard</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#interfaces-links"' :
                            'data-target="#xs-interfaces-links"' }>
                            <span class="icon ion-md-information-circle-outline"></span>
                            <span>Interfaces</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? ' id="interfaces-links"' : 'id="xs-interfaces-links"' }>
                            <li class="link">
                                <a href="interfaces/DataRef.html" data-type="entity-link">DataRef</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#miscellaneous-links"'
                            : 'data-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Miscellaneous</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/enumerations.html" data-type="entity-link">Enums</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/functions.html" data-type="entity-link">Functions</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/typealiases.html" data-type="entity-link">Type aliases</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/variables.html" data-type="entity-link">Variables</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Documentation coverage</a>
                    </li>
                    <li class="divider"></li>
                    <li class="copyright">
                        Documentation generated using <a href="https://compodoc.app/" target="_blank">
                            <img data-src="images/compodoc-vectorise.png" class="img-responsive" data-type="compodoc-logo">
                        </a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});